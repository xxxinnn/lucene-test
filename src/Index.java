import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.ja.*;
import org.apache.lucene.analysis.ja.dict.*;
import org.apache.lucene.analysis.standard.*;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.store.*;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.text.*;
import org.apache.poi.*;
import org.apache.poi.hwpf.extractor.*;
import org.apache.poi.openxml4j.opc.*;
import org.apache.poi.poifs.filesystem.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xwpf.extractor.*;

import java.io.*;
import java.nio.file.*;

class Index {
    /**
     * create index for search
     * @param indexPath the directory where will create the index file
     * @param dataPath the directory that have the data,
     */
    public void index(String indexPath,String dataPath) {
        File index = new File(indexPath);
        if(!index.exists()){
            index.mkdirs();
        }
        if(new File(dataPath).exists()) {
            IndexWriter indexWriter = null;
            try {
                System.out.println("Creating index");
                // 1、创建Directory
                Directory directory = FSDirectory.open(FileSystems.getDefault().getPath(indexPath));
                // 2、创建IndexWriter
                Analyzer analyzer = new StandardAnalyzer();
//                  Analyzer analyzer = new JapaneseAnalyzer(null, JapaneseTokenizer.Mode.SEARCH,JapaneseAnalyzer.getDefaultStopSet(),JapaneseAnalyzer.getDefaultStopTags());

//                CharArraySet charArraySet = new CharArraySet(2,true);
//                analyzer = new JapaneseAnalyzer(UserDictionary.open(new FileReader()), JapaneseTokenizer.Mode.SEARCH,charArraySet,new HashSet<String>());

                IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
                indexWriter = new IndexWriter(directory, indexWriterConfig);
                indexWriter.deleteAll();//清除以前的index
                //要搜索的File路径
                File dFile = new File(dataPath);
                File[] files = dFile.listFiles();
                setIndex(indexWriter, files);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (indexWriter != null) {
                        indexWriter.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //递归 遍历文件夹
    private static IndexWriter setIndex(IndexWriter indexWriter, File[] files)throws Exception{
        if (files.length >0) {
            for (File file : files) {
                if (file.isDirectory()){
                    //目录
                    setIndex(indexWriter,file.listFiles());
                }
                if (file.isFile()) {
                    //文件
                    Document document = new Document();
                    String filename = file.getName();
                    System.out.println(file.getPath());
                    //不同类型的文件
                    if (filename.endsWith("pdf")) {
                        //PDF
                        document.add(new Field("content", readPdf(file.getPath()), TextField.TYPE_STORED));
                    } else if (filename.endsWith("doc") || filename.endsWith("docx")) {
                        //doc docx
                        document.add(new Field("content", readWord(file.getPath()), TextField.TYPE_STORED));
                    } else if (filename.endsWith("xlsx") || filename.endsWith("xls")) {
                        //xlsx xls
                        document.add(new Field("content", readExcel(file.getPath()), TextField.TYPE_STORED));
                    } else if (filename.endsWith("txt")){
                        //txt other
                        document.add(new Field("content", readTxt(file.getPath()), TextField.TYPE_STORED));
                    }
                    document.add(new Field("filename", file.getName(), TextField.TYPE_STORED));
                    document.add(new Field("filepath", file.getAbsolutePath(), TextField.TYPE_STORED));

                    indexWriter.addDocument(document);
                }
            }
        }
        return indexWriter;
    }

    /**
     * read PDF file and transform to String
     * @param path file path
     * @return String
     * @throws Exception
     */
    public static String readPdf(String path)throws Exception{
        //use pdfbox
        StringBuilder content = new StringBuilder("");
        FileInputStream fis = null;
        PDDocument doc = new PDDocument();
        doc.close();
        StringWriter writer = new StringWriter();
        try {
            fis = new FileInputStream(path);
            PDFTextStripper ts = new PDFTextStripper();
            doc = PDDocument.load(fis);
            ts.writeText(doc,writer);
            content.append(writer.getBuffer().toString());
        }catch (Exception e){
//            e.printStackTrace();
        }finally {
            if(fis!=null) {
                fis.close();
            }
            writer.close();
            doc.close();
        }
        return content.toString().trim();
    }

    public static String readWord(String path)throws Exception{
        StringBuilder content = new StringBuilder("");
        FileInputStream fis = null;
        try{
            //03以下的word
            fis = new FileInputStream(path);
            WordExtractor ex = new WordExtractor(fis);
            content.append(ex.getText());
        }catch (OfficeXmlFileException e){
            //07以上
            OPCPackage opcPackage = POIXMLDocument.openPackage(path);
            POIXMLTextExtractor extractor = new XWPFWordExtractor(opcPackage);
            content.append(extractor.getText());
        }catch (Exception e){
//            e.printStackTrace();
        }finally {
            if(fis!=null){
                fis.close();
            }
        }
        return content.toString().trim();
    }

    public static String readExcel(String path)throws Exception{
        FileInputStream fis = null;
        Workbook wb = null;
        StringBuilder sb = new StringBuilder();
        try{
            fis = new FileInputStream(path);
            wb = WorkbookFactory.create(fis);
            for(int i=0; i< wb.getNumberOfSheets()-1; i++){
                Sheet sheet = wb.getSheetAt(i);
                for(Row r : sheet) {
                    for(Cell cell : r){
                        CellType cellType = cell.getCellTypeEnum();
                        switch(cellType){
                            case FORMULA:
                                sb.append(cell.getCellFormula());
                                break;
                            case BOOLEAN:
                                sb.append(cell.getBooleanCellValue());
                                break;
                            case NUMERIC:
                                sb.append(cell.getNumericCellValue());
                                break;
                            case STRING:
                                sb.append(cell.getStringCellValue());
                                break;
                            case ERROR:
                                sb.append(cell.getErrorCellValue());
                                break;
                            case BLANK:
                                break;
                            case _NONE:
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }catch (Exception e){
//            e.printStackTrace();
        }finally {
            if (wb != null) {
                wb.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return sb.toString().trim();
    }
    public static String readTxt(String path)throws Exception{
        BufferedReader br=new BufferedReader(new FileReader(path));
        String line="";
        StringBuffer buffer = new StringBuffer();
        while((line=br.readLine())!=null){
            buffer.append(line);
        }
        String content = buffer.toString();
        return content;
    }
}
