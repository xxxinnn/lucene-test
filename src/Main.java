import java.io.*;

public class Main {

    public static void main(String args[]) throws Exception {
//        索引文件目录
        String indexPath = "C:/Users/Administrator/Desktop/lucene/index";
//        检索文件目录
        String dataPath = "C:/Users/Administrator/Desktop/lucene/data";
//        结果数
        int num = 10;
        String keyword = "価 科学";

        //支持txt、pdf、xls、xlsx、doc、docx格式
        //英文会去除the a an am is are will之类的
        //日文需要全匹配
        //数字需要整串匹配 如321 1234只能匹配【321】或【1234】
        //混合搜索 英文及数字需全匹配

        //全角 半角？
        //读音 汉字？
        //平假名 片假名？

        //create index
        Index newIndex = new Index();
        newIndex.index(indexPath,dataPath);
        //search
        Search newSearch = new Search();
        newSearch.search(keyword,indexPath,num);
        boolean cont = true;
        while(cont){
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            keyword = br.readLine();
            if(keyword.equals("q")) cont=false;
            newSearch.search(keyword,indexPath,num);
        }

    }

}