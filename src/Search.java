import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.en.*;
import org.apache.lucene.analysis.ja.*;
import org.apache.lucene.analysis.standard.*;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.*;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.store.*;

import java.io.*;
import java.nio.file.*;

class Search {
    /**
     * search the index with keyword
     * @param keyWord the word to be searched
     * @param indexPath the index files' directory
     * @param num the number of result
     */
    public void search(String keyWord, String indexPath, int num) {
        DirectoryReader directoryReader = null;
        if(new File(indexPath).exists()) {
            try {
                Directory directory = FSDirectory.open(FileSystems.getDefault().getPath(indexPath));
                directoryReader = DirectoryReader.open(directory);
                IndexSearcher indexSearcher = new IndexSearcher(directoryReader);

//                    Analyzer analyzer = new StandardAnalyzer();
                Analyzer analyzer = new JapaneseAnalyzer();
//                Analyzer analyzer = new JapaneseAnalyzer(null, JapaneseTokenizer.Mode.SEARCH,JapaneseAnalyzer.getDefaultStopSet(),JapaneseAnalyzer.getDefaultStopTags());

                //field to be searched
                PhraseQuery.Builder builder = new PhraseQuery.Builder();
                TokenStream token = analyzer.tokenStream("content", keyWord);
                JapaneseReadingFormFilter a = new JapaneseReadingFormFilter(token);


                if(keyWord.trim().matches("^[a-zA-Z]*")) {
                    //英文
                    analyzer = new EnglishAnalyzer();
                    builder.add(new Term("content", keyWord.trim()));
                } else{
                    //日文 每个词输入
                    analyzer = new StandardAnalyzer();
                    builder.setSlop(0);
                    for (int i = 0; i < keyWord.trim().length(); i++) {
                        builder.add(new Term("content", String.valueOf(keyWord.charAt(i))));
                    }
                }
                Query query = builder.build();

                TopDocs topDocs = indexSearcher.search(query, num);
                ScoreDoc[] scoreDocs = topDocs.scoreDocs;
                int count = 0;
                //没有匹配、尝试QueryParser混合搜索
                if(scoreDocs.length< 1) {
                    analyzer = new JapaneseAnalyzer();
                    System.out.println("TRYING ANOTHER WAY...");
                    QueryParser queryParser = new QueryParser("content", analyzer);
//                    query = queryParser.parse(keyWord);
                    query = queryParser.parse(keyWord);
                    topDocs = indexSearcher.search(query, num);
                    scoreDocs = topDocs.scoreDocs;
                }
                System.out.println("keyword:" + keyWord );
                System.out.println("the number files totally found ：" + topDocs.totalHits);

                for (ScoreDoc scoreDoc : scoreDocs) {

                    Document document = indexSearcher.doc(scoreDoc.doc);
                    System.out.println(++count + "、" + document.get("filepath"));
                    //摘要
                    Formatter formatter = new SimpleHTMLFormatter("[", "]");
                    QueryScorer source = new QueryScorer(query);
                    Highlighter highlighter = new Highlighter(formatter, source);
                    String text = highlighter.getBestFragment(analyzer, "content", document.get("content"));
                    System.out.println("score: " + scoreDoc.score);
                    System.out.println("parts: " + text);
                    System.out.println();

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (directoryReader != null) {
                        directoryReader.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}