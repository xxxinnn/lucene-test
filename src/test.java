import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.ja.*;
import org.apache.lucene.analysis.ja.tokenattributes.*;
import org.apache.lucene.analysis.standard.*;
import org.apache.lucene.analysis.tokenattributes.*;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.*;

import java.io.*;
import java.nio.file.*;

/**
 * Created by Administrator on 2017/6/12.
 */
public class test {
    public static void main(String args[]) throws Exception {
//        索引文件目录
        String indexPath = "C:/Users/Administrator/Desktop/lucene/index";
//        检索文件目录
        String dataPath = "C:/Users/Administrator/Desktop/lucene/data";
        String source = "私は毎晩サカナを食べます、１２３４さかなはBIG123魚です   貴方の電話番号は３２１・３１２";
        String txtfile = "C:/Users/Administrator/Desktop/lucene/data/問い.txt";
        String docfile = "C:/Users/Administrator/Desktop/lucene/data/日本.doc";

//        Index newIndex = new Index();
//        newIndex.index(indexPath,dataPath);
//        Tokenizer tokenizer = new StandardTokenizer();
        Analyzer analyzer = new JapaneseAnalyzer();
        Analyzer analyzer1 = new StandardAnalyzer();
        showToken(source,analyzer);
        showToken(source,analyzer1);


        Index index = new Index();

        String txt = index.readTxt(txtfile);
        showToken(txt,analyzer);
        showToken(txt,analyzer1);

//        String doc = index.readWord(docfile);
//        showToken(doc,analyzer);
//        showToken(doc,analyzer1);
    }

    //分词情况
    public static void showToken(String source,Analyzer analyzer) throws IOException {
        TokenStream stream = analyzer.tokenStream("",new StringReader(source));
//        BoostAttribute boostAttribute = stream.addAttribute(BoostAttribute.class);
//        BytesTermAttribute bytesTermAttribute = stream.addAttribute(BytesTermAttribute.class);
        //分词情况
        CharTermAttribute charTermAttribute = stream.addAttribute(CharTermAttribute.class);
//        FlagsAttribute flagsAttribute = stream.addAttribute(FlagsAttribute.class);
//        FuzzyTermsEnum.LevenshteinAutomataAttribute levenshteinAutomataAttribute = stream.addAttribute(FuzzyTermsEnum.LevenshteinAutomataAttribute.class);
        KeywordAttribute keywordAttribute = stream.addAttribute(KeywordAttribute.class);
//        LegacyNumericTokenStream.LegacyNumericTermAttribute legacyNumericTermAttribute = stream.addAttribute(LegacyNumericTokenStream.LegacyNumericTermAttribute.class);
        TypeAttribute typeAttribute = stream.addAttribute(TypeAttribute.class);
//        TermToBytesRefAttribute termToBytesRefAttribute = stream.addAttribute(TermToBytesRefAttribute.class);
        PositionIncrementAttribute positionIncrementAttribute = stream.addAttribute(PositionIncrementAttribute.class);
        PositionLengthAttribute positionLengthAttribute = stream.addAttribute(PositionLengthAttribute.class);
        //读音
        ReadingAttribute readingAttribute = stream.addAttribute(ReadingAttribute.class);

        stream.reset();
        while(stream.incrementToken()){
            System.out.print("["+charTermAttribute+"]");
//            System.out.print("["+readingAttribute.getPronunciation()+"]");
//            System.out.print("["+readingAttribute.getReading()+"]");
//            System.out.print("["+keywordAttribute.isKeyword()+"]");
//            System.out.print("["+typeAttribute+"]");
//            System.out.print("["+positionLengthAttribute.getPositionLength()+"]");
//            System.out.print("["+positionIncrementAttribute.getPositionIncrement()+"]");
        }
        stream.close();
        System.out.println();
    }

    public static void search(Analyzer analyzer,String indexPath, String dataPath,String keyWord)throws Exception{
        new Index().index(indexPath,dataPath);

        Directory directory = FSDirectory.open(FileSystems.getDefault().getPath(indexPath));
        DirectoryReader directoryReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(directoryReader);

        //1、TermQuery最基本的查询
//        Query query = new TermQuery(new Term("content","123"));
        //2、BooleanQuery多种条件如BooleanClause.Occur.SHOULD MUST
        //and用+，or用 表示
        //3、WildcardQuery 通配符*?
        //4、PhraseQuery可设置间距slop 可使用~
        PhraseQuery.Builder builder = new PhraseQuery.Builder();
        builder.add(new Term("content","1"));//关键词
        builder.setSlop(0);//间距
        Query query = builder.build();
        //5、PrefixQuery以..开头*
        //6、FuzzyQuery相似~
        //7、RangeQuery范围

        //

//        QueryParser queryParser = new QueryParser("content", analyzer);
////                    query = queryParser.parse(keyWord);
//        query = queryParser.parse("+(content:"+keyWord+")");

        TopDocs topDocs = indexSearcher.search(query, 10);
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        topDocs = indexSearcher.search(query, 10);
        scoreDocs = topDocs.scoreDocs;

        System.out.println("keyword:" + keyWord );
        System.out.println("the number files totally found ：" + topDocs.totalHits);

        int count = 0;
        for (ScoreDoc scoreDoc : scoreDocs) {

            Document document = indexSearcher.doc(scoreDoc.doc);
            System.out.println(++count + "、" + document.get("filepath"));
            //摘要
            Formatter formatter = new SimpleHTMLFormatter("[", "]");
            QueryScorer source = new QueryScorer(query);
            Highlighter highlighter = new Highlighter(formatter, source);
            String text = highlighter.getBestFragment(analyzer, "content", document.get("content"));
            System.out.println("score: " + scoreDoc.score);
            System.out.println("parts: " + text);
            System.out.println();
        }
    }

}
